//
//  MessagesViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class MessagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var tblView: UITableView!
    var ref: DatabaseReference!
    var friendsArray = [UserModel]()
    
    let storage = Storage.storage()
    var userName: String?
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        animateTableView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.navigationController?.navigationBar.isHidden = true
        title = "Messenger"
        ref = Database.database().reference()
        tblView.tableFooterView = UIView()
        readUsers()
        swipes()
        tblView.rowHeight = 117
        // Do any additional setup after loading the view.
    }
    func swipes() {
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe :)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(rightSwipe)
        
    }
    
    @objc func swipeAction(swipe: UISwipeGestureRecognizer) {
        switch swipe.direction.rawValue {
        case 1:
            tabBarController?.selectedIndex -= 1
        default:
            break
        }
    }
    func readUsers(){
        self.ref.child("publicUsers").observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let users = snapshot.value as? [String: [String:Any]] else{
                return
            }
            for user in users {
                
                let names = user.value
                print(names)
                if user.key == Auth.auth().currentUser?.uid{
                    self.userName = names["firstName"] as? String
                    print(self.userName)
                }
                else if user.key != Auth.auth().currentUser?.uid {
                    if let fname = names["firstName"],let lname = names["lastName"] {
                        print(fname,lname)
                        self.friendsArray.append(UserModel(fname: fname as! String, lname: lname as! String, userId: user.key))
                    }
                    
                }
            }
        })
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as? MessageTableViewCell
        
        let friendObj = friendsArray[indexPath.row]
        let uid = friendObj.uId
        let storageRef = storage.reference()
        let imageRef = storageRef.child("images/\(uid).jpg")
        cell?.lblTxt.text = friendObj.firstName + " " + friendObj.lastName
        imageRef.getData(maxSize: 1 * 2048 * 1024) { (data, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }else{
                cell?.imgView.image = UIImage(data: data!)
            }
        }
        return cell!
    }
    
    func animateTableView() {
        tblView.reloadData()
        
        let tableViewHeight = tblView.bounds.size.height
        let cells = tblView.visibleCells
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delay = 0
        
        for cell in cells {
            
            UIView.animate(withDuration: 1.5, delay: Double(delay) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delay += 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let friendObj = friendsArray[indexPath.row]
        if let controller = storyboard?.instantiateViewController(withIdentifier: "MessagingViewController") as? MessagingViewController {
            controller.friendName = friendObj.firstName
            controller.friendid = friendObj.uId
            controller.userName = userName
            print(controller.userName)
            print(controller.friendid)
            print(controller.friendName)
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
