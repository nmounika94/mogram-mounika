//
//  MessageModel.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
import JSQMessagesViewController

struct MessagesModel{
    
    var msg : JSQMessage
    var ts : Int
}
struct User {
    let id: String
    let name: String
}
