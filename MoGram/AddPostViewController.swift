//
//  AddPostViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

protocol  AddPostVcToPostsTVc: class {
    func displaynewPost()
}
class AddPostViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    var pickedImage: UIImage?
    var ref: DatabaseReference!
    let storage = Storage.storage()

    @IBOutlet weak var descpTxt: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    var delegate : AddPostVcToPostsTVc?
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        imgView.image = pickedImage
        
        //let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handlepic))
//        imgView.addGestureRecognizer(tapGesture)
//        imgView.isUserInteractionEnabled = true
//
        // Do any additional setup after loading the view.
    }
    @IBAction func pickImgBtn(_ sender: UIButton) {
       
//        let imgPicker = UIImagePickerController()
//        imgPicker.delegate = self
//        imgPicker.allowsEditing = true
//        imgPicker.sourceType = .photoLibrary
//        present(imgPicker, animated: true, completion: nil)
    }
//    @objc func handlepic(){
//
//    let pickerController = UIImagePickerController()
//        pickerController.delegate = self
//    }
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        if let img = info[UIImagePickerControllerEditedImage] as? UIImage {
//
//            imgView.image = img
//
//        }
//        dismiss(animated: true, completion: nil)
//    }

    @IBAction func postBtn(_ sender: UIBarButtonItem) {
        
        let key = ref.child("posts").childByAutoId().key
        guard let userID = Auth.auth().currentUser?.uid  else {
            return
        }
        let description = descpTxt.text
        let post = ["uid": userID,"description": description ?? "No description","image": "postImages/\(key).jpg","timeStamp" : timeStamp()] as [String: Any]
        let childUpdate1 = ["/posts/\(key)":post]
        let childUpdate2 = ["/publicUsers/\(userID)/posts/\(key)/": true]
        print(childUpdate1)
        print(childUpdate2)
        uploadImage(key)
        ref.updateChildValues(childUpdate1)
        ref.updateChildValues(childUpdate2)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "PostsTableViewController") as? PostsTableViewController{
            present(controller, animated: true, completion: nil)
        }
        
        navigationController?.popToRootViewController(animated: true)
    }
    
    func timeStamp() -> Double {
        
        let timeStamp = Date().timeIntervalSince1970
        let reversedTimeStamp = -1.0 * timeStamp
        print((reversedTimeStamp))
        return reversedTimeStamp
    }
    
    func uploadImage(_ postId : String) {
        
        let imgData = UIImageJPEGRepresentation(imgView.image!, 0.2)
        let storageRef = storage.reference()
        let imageRef = storageRef.child("postImages/\(postId).jpg")
        let _ = imageRef.putData(imgData!, metadata: nil) { (metadata, error) in
            guard let _ = metadata else {
                print("error")
                return
            }
            print("image Uploaded")
            self.delegate?.displaynewPost()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
