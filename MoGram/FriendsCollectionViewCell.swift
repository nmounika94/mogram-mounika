//
//  FriendsCollectionViewCell.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class FriendsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
}
