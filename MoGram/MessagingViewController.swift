//
//  MessagingViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import JSQMessagesViewController

class MessagingViewController:JSQMessagesViewController  {
    
    var friendName: String?
    var friendid: String?
    var userName: String?
    let userId = Auth.auth().currentUser?.uid
    var covoId: String?
    var ref: DatabaseReference!
    var currentUser : User?
    var friendUser: User?
    var messages = [MessagesModel]()
    var rawMessages = [MessagesModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userId! < friendid! {
            covoId = "\(userId!)\(friendid!)"
        }
        else{
            covoId = "\(friendid!)\(userId!)"
        }
        
        senderId = userId
        senderDisplayName = userName
        print(senderDisplayName)
        print(senderId)
        currentUser = User(id: userId!, name: userName!)
        friendUser = User(id: friendid!, name: friendName!)
        readMessages()
        // Do any additional setup after loading the view.
    }
    
    func readMessages() {
      
        ref = Database.database().reference()
        ref.child("messages").child(covoId!).observe(.value, with: { (snapshot) in
            
            self.messages = []
            self.rawMessages = []
            guard let msgs = snapshot.value as? [String: Any] else{
                return
            }
            
            for msg in msgs{
                let ts = msg.key
                let text = msg.value as? [String: Any]
                
                let message = JSQMessage(senderId: text?["senderId"] as! String, displayName: text?["displayName"] as! String, text: text?["text"] as! String)
                self.rawMessages.append(MessagesModel(msg: message!, ts: Int(ts)!))
            }
            
            self.messages = self.rawMessages.sorted(by: { (msg1, msg2) -> Bool in
                let ts1 = msg1.ts
                let ts2 = msg2.ts
                
                return ts1>ts2
            })
            
            self.collectionView.reloadData()
        })
    }
    
    func timeStamp()-> Double {
        let timeStamp = Date().timeIntervalSince1970
        let reversedTimeStamp = -1.0 * timeStamp
        return reversedTimeStamp
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MessagingViewController{
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        let ts = Int(timeStamp())
        let msg = ["senderId": message!.senderId,
                   "displayName": message!.senderDisplayName,
                   "text" : message!.text] as [String : Any]
        ref.child("messages/\(covoId!)/\(ts)").setValue(msg)
        finishSendingMessage()
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return messages[indexPath.row].msg
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        scrollToBottom(animated: true)
        let message = messages[indexPath.row].msg
        let msgUserName = message.senderDisplayName
        
        return NSAttributedString(string: msgUserName!)
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row].msg
        
        if currentUser?.id == message.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor.blue)
        }
            
        else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor.green)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    
    
}
    
   

