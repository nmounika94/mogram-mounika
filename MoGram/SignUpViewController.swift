//
//  SignUpViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/24/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var reenterPswdTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    @IBAction func signUpBtn(_ sender: UIButton) {
        
        let password = passwordTF.text
        let email = emailTF.text
        let reenterPswd = reenterPswdTF.text
        
        if password != reenterPswd{
            passwordTF.text = ""
            reenterPswdTF.text = ""
            return
        }
        
        Auth.auth().createUser(withEmail: email!, password: password!) { (user, error) in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            print("User Created")
            
            if let controller = self.storyboard?.instantiateViewController(withIdentifier:"TabBarController"){
                self.present(controller,animated: true,completion: nil)
                self.passwordTF.text = ""
                self.reenterPswdTF.text = ""
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
