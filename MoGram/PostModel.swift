//
//  PostModel.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
class PostModel {
    var desc: String?
    var img: String?
    var userName: String?
    var uId: String?
    var isliked: Bool?
    var key : String?
    var likecount: Int?
    var timeStamp: Double?
    
    init(descrip: String,image: String,name: String,userId: String,liked: Bool,postKey: String,count: Int,tstamp: Double) {
        
        desc = descrip
        img = image
        userName = name
        uId = userId
        isliked = liked
        key = postKey
        likecount = count
        timeStamp = tstamp
    }
    
}
