//
//  PostsTableViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import SVProgressHUD

class PostsTableViewController: UITableViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AddPostVcToPostsTVc {
    func displaynewPost() {
        tableView.reloadData()
    }
    
    
    var ref: DatabaseReference!
    var friendsArray = [UserModel]()
    var postarray = [PostModel]()
    let refresh = UIRefreshControl()
    let storage = Storage.storage()
    var sortedPostArray = [PostModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Feed"
        ref = Database.database().reference()
        readPosts()
        readUsers()
        
        tableView.rowHeight = 519
    }
    
    func swipes() {
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        view.addGestureRecognizer(leftSwipe)
    }
    
    @objc func swipeAction( swipe : UISwipeGestureRecognizer) {
        
        switch  swipe.direction.rawValue {
        case 2:
            tabBarController?.selectedIndex += 1
        default:
            break
        }
    }
    
    func refreshAction() {
        
        SVProgressHUD.show()
        tableView.reloadData()
        SVProgressHUD.dismiss()
        self.refresh.endRefreshing()
    }
    
    func readPosts() {
        
        self.ref.child("posts").queryOrdered(byChild: "timeStamp").observe(.value,with: { (snapshot) in
            
            self.postarray = []
            var username: String
            
            guard let object = snapshot.value as? [String: [String: Any]] else{
                return
            }
            
            for posts in object {
                var count = 0
                let currentUid = Auth.auth().currentUser?.uid
                var liked = false
                var key = posts.key
                print(key)
                let post = posts.value
                print(post)
                
                let description = post["decsription"] ?? "no decsription"
                let timeStamp = post["timeStamp"] ?? 0
                let userID = post["uid"] ?? ""
                let imageUrl = post["image"] ?? ""
                if let likes = post["likes"] as? [String: Bool] {
                    for like in likes {
                        
                        if like.value == true{
                             count += 1
                        }
                        
                        if like.key == currentUid {
                            if like.value == true {
                                liked = true
                            }
                           
                        }
                        
                    }
                }
                
                for obj in self.friendsArray {
                    if obj.uId == "\(userID)" {
                        username = "\(obj.firstName)"
                        self.postarray.append(PostModel(descrip: description as! String, image: imageUrl as! String, name: username as! String, userId: userID as! String, liked:liked , postKey: key, count: count, tstamp: timeStamp as! Double))
                        
                    }
                }
                
                self.sortedPostArray = self.postarray.sorted(by: { (item1, item2) -> Bool in
                    
                    let ts1 = item1.timeStamp!
                    let ts2 = item2.timeStamp!
                    
                    return(ts1 < ts2)
                })
            }
            
            SVProgressHUD.dismiss()
            self.refresh.endRefreshing()
            self.tableView.reloadData()
        })
    }
    
    func readUsers() {
        
        self.ref.child("publicUsers").observeSingleEvent(of: .value,with: { (snapshot) in
            guard let users = snapshot.value as? [String:[String: Any]] else{
                return
            }
            for user in users {
                let names  = user.value
                
                if let fname = names["firstName"], let lname = names["lastName"] {
                    self.friendsArray.append(UserModel(fname: fname as! String, lname: lname as! String, userId: user.key))
                }
            }
            
            print(self.friendsArray.count)
            self.tableView.reloadData()
        })
    }
    
    @IBAction func addPostBtn(_ sender: UIBarButtonItem) {
    
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)

//        if let controller = storyboard?.instantiateViewController(withIdentifier: "AddPostViewController") as? AddPostViewController {
//
//            //controller.pickedImage = img
//            navigationController?.pushViewController(controller, animated: true)
//        }
//
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let img = info[UIImagePickerControllerEditedImage] as? UIImage else {
            return
        }
        
        print(img)
        
        dismiss(animated: true, completion: nil)
        if let controller = storyboard?.instantiateViewController(withIdentifier: "AddPostViewController") as? AddPostViewController{
           
            controller.pickedImage = img
            navigationController?.pushViewController(controller, animated: true)
        }
        
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(sortedPostArray.count)
        // #warning Incomplete implementation, return the number of rows
        return sortedPostArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDisplayTableViewCell", for: indexPath) as? FeedDisplayTableViewCell
        
        let postObj = sortedPostArray[indexPath.row]
        let uid = postObj.uId
        let storageRef = storage.reference()
        let dp = storageRef.child("images/\(uid!).jpg")
        let postPic = storageRef.child((postObj.img)!)
        postPic.getData(maxSize: 1 * 2048 * 1024) { (data, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }
            else{
                cell?.postImgView.image = UIImage(data: data!)
            }
        }
        dp.getData(maxSize: 1 * 2048 * 1024) { (data, error) in
            
            if let error = error {
                
                print(error.localizedDescription)
            }
            else{
                cell?.displayImgView.image = UIImage(data: data!)
            }
        }
        let liked = postObj.isliked
        if liked == true {
            cell?.heartBtn.isSelected = true
            cell?.heartBtn.setImage(UIImage(named: "liked"), for: .normal)
        }
        else {
            cell?.heartBtn.isSelected = false
            cell?.heartBtn.setImage(UIImage(named: "unliked"), for: .normal)
        }
        
        cell?.userNameLabel.text = postObj.userName
        cell?.descripLabel.text = postObj.desc
        cell?.key = postObj.key
        cell?.uid = Auth.auth().currentUser?.uid
        cell?.likesLabel.text = "\(postObj.likecount ?? 0)"

        // Configure the cell...

        return cell!
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
