//
//  LoginViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var emailIdTf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func loginBtn(_ sender: UIButton) {
        
        let password = passwordTf.text
        let email = emailIdTf.text
        
        Auth.auth().signIn(withEmail: email!, password: password!) { (user, error) in
            
            if let error = error {
                print(error.localizedDescription)
        }
            
            print("User Logged in")
            
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") {
                
                self.present(controller, animated: true, completion: nil)
                self.passwordTf.text = ""
            }
    }
    }
    @IBAction func SignUpBtn(_ sender: UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
