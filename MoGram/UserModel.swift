//
//  UserModel.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
class UserModel{
    var firstName : String
    var lastName: String
    var uId: String
    init(fname: String,lname: String,userId: String) {
        firstName = fname
        lastName = lname
        uId = userId
    }
}
