//
//  FeedDisplayTableViewCell.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase

class FeedDisplayTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var displayImgView: UIImageView!
    @IBOutlet weak var postImgView: UIImageView!
    @IBOutlet weak var heartBtn: UIButton!
    
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var descripLabel: UILabel!
    let storage = Storage.storage()
    
    var ref: DatabaseReference!
    var key : String?
    var uid: String?
    var liked: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.layer.cornerRadius = 5
        cellView.layer.shadowColor = UIColor.black.cgColor
        cellView.layer.shadowOpacity = 0.5
        cellView.layer.shadowOffset = CGSize.zero
        cellView.layer.shadowRadius = 10
        ref = Database.database().reference()
        displayImgView.layer.cornerRadius = 20
    }

    @IBAction func likesBtn(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true {
            
            ref.child("posts/\(key!)/likes/\(uid!)").setValue(true)
            sender.setImage(UIImage(named: "liked"), for: .normal)
        }
        else {
            ref.child("posts/\(key!)/likes/\(uid!)").setValue(false)
            sender.setImage(UIImage(named: "unkied"), for: .normal)
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
