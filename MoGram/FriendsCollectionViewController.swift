//
//  FriendsCollectionViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/26/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

import Firebase
import FirebaseStorage

class FriendsCollectionViewController: UICollectionViewController {

    
    var ref: DatabaseReference!
    var friendsArray = [String]()
    var userArray = [UserModel]()
    let refresh = UIRefreshControl()
    let storage = Storage.storage()
    var friendObjects = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Friends"
        collectionView?.alwaysBounceVertical = true
        refresh.isEnabled = true
        refresh.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        collectionView?.addSubview(refresh)
        readCurrentUserFriends()
        readUsers()
        ref = Database.database().reference()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        swipes()
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshAction(){
        
        friendsArray = []
        userArray = []
        friendObjects = []
        readCurrentUserFriends()
    }
    
    func swipes() {
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe :)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe :)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(rightSwipe)
        
    }
    
    @objc func swipeAction(swipe: UISwipeGestureRecognizer) {
        
        switch swipe.direction.rawValue {
        case 1:
            tabBarController?.selectedIndex -= 1
        case 2:
            tabBarController?.selectedIndex += 1
        default:
            break
        }
        
    }
    
    
    func readCurrentUserFriends() {
        
        self.ref.child("publicUsers").observe(.value, with:{ (snapshot) in
            
            self.friendsArray = []
            
            guard let users = snapshot.value as? [String:[String: Any]] else{
                return
            }
            
            for user in users {
                
                let names = user.value
                if user.key == Auth.auth().currentUser?.uid {
                    
                    if let friends = names["friends"] as? [String: Bool] {
                        for friend in friends {
                            
                            if friend.value == true {
                                self.friendsArray.append(friend.key)
                            }
                        }
                    }
                }
            }
            
            self.collectionView?.reloadData()
        })
    }
    
    func readUsers(){
        
        self.ref.child("publicUsers").observe(.value, with:{ (snapshot) in
            
            self.userArray = []
            guard let users = snapshot.value as? [String:[String: Any]] else{
                return
            }
            
            for user in users {
                
                let names = user.value
                if user.key != Auth.auth().currentUser?.uid {
                    
                    if let fname = names["firstName"], let lname = names["lastName"] {
                        self.userArray.append(UserModel(fname: fname as! String, lname: lname as! String, userId: user.key))
                    }
                }
            }
            
            self.refresh.endRefreshing()
            self.filterFriends()
            self.collectionView?.reloadData()
        })
    }
    
    func filterFriends() {
        
        friendObjects = []
        for friend in userArray {
            if friendsArray.contains(friend.uId) {
                friendObjects.append(friend)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return friendObjects.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendsCollectionViewCell", for: indexPath) as? FriendsCollectionViewCell
        
        cell?.imgView.layer.cornerRadius = 40
        let friendsobj = friendObjects[indexPath.row]
        let uid = friendsobj.uId
        let storageRef = storage.reference()
        let imageRef = storageRef.child("images/\(uid).jpg")
        imageRef.getData(maxSize: 1 * 2048 * 1024) { (data, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }else {
                cell?.imgView.image = UIImage(data: data!)
            }
        }
       cell?.nameLbl.text = "\(friendsobj.firstName) \(friendsobj.lastName)"
    
        return cell!
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
