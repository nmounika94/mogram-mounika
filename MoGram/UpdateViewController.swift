//
//  UpdateViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class UpdateViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    var ref: DatabaseReference!
    let storage = Storage.storage()

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var ageTf: UITextField!
    @IBOutlet weak var ssnTf: UITextField!
    @IBOutlet weak var lastNameTf: UITextField!
    @IBOutlet weak var frstNameTf: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Update Profile"
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }

    @IBAction func pickImageBtn(_ sender: UIButton) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
   
    @IBAction func updateBtn(_ sender: UIButton) {
        guard let lastName = lastNameTf.text,let firstName = frstNameTf.text,let age = ageTf.text,let ssn = ssnTf.text,let image = imgView.image else {
            print("Any one of the textField is nill")
            return
        }
        
       let cUid = (Auth.auth().currentUser?.uid)!
        
        ref.child("users").child((Auth.auth().currentUser?.uid)!).setValue(["firstName" : firstName, "lastName": lastName,"age": age,"ssn": ssn])
        ref.child("publicUsers/\(cUid)/firstName").setValue(firstName)
        ref.child("publicUsers/\(cUid)/lastName").setValue(lastName)
        
        uploadImage(image)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func removeBtn(_ sender: UIButton) {
        
        self.removingData()
    }
    func uploadImage( _ img : UIImage) {
        
        let imgData = UIImageJPEGRepresentation(img, 1)
        let storageRef = storage.reference()
        let uid = Auth.auth().currentUser!.uid
        let imageRef = storageRef.child("images/\(uid).jpg")
        let _ = imageRef.putData(imgData!, metadata: nil) { (metadata, error) in
            guard let _ = metadata else{
                print("error")
                return
            }
            
            print("imageUploaded")
        }
    }
    
    func removingData() {
        self.txtView.text = ""
        self.imgView.image = UIImage(named: "")
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerEditedImage] as? UIImage {
            imgView.image = img
        }
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
