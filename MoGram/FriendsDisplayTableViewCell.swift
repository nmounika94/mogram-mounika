//
//  FriendsDisplayTableViewCell.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class FriendsDisplayTableViewCell: UITableViewCell {

    @IBOutlet weak var addFrndBtn: UIButton!
    @IBOutlet weak var lbltxt: UILabel!
    @IBOutlet weak var imgview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
