//
//  FiltersViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import SnapSliderFilters

class FiltersViewController: UIViewController {

    
    var pickedImage : UIImage?
    let screenView:UIView = UIView(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    var slider = SNSlider(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    let textField = SNTextField(y: SNUtils.screenSize.height/2, width: SNUtils.screenSize.width, heightOfScreen: SNUtils.screenSize.height)
    let tapGesture = UITapGestureRecognizer()
    let buttonSave = SNButton(frame: CGRect(x: 20, y: SNUtils.screenSize.height - 35, width: 33, height: 30), withImageNamed: "saveButton")
    let buttonCamera = SNButton(frame: CGRect(x: 75, y: SNUtils.screenSize.height - 42, width: 45, height: 45), withImageNamed: "galleryButton")
    // Generate differents filters by passing in argument the original picture and an array of filter's name
    var data:[SNFilter] = []
    let imagePicker = UIImagePickerController()
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = true
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGesture.addTarget(self, action: #selector(handleTap))
        
        setupSlider()
        setupTextField()
        view.addSubview(screenView)
        setupButtonSave()
        setupButtonCamera()
        // Do any additional setup after loading the view.
    }
    func createData(_ image: UIImage) {
        self.data = SNFilter.generateFilters(SNFilter(frame: self.slider.frame, withImage: image), filters: SNFilter.filterNameList)
        
        self.data[1].addSticker(SNSticker(frame: CGRect(x: 195, y: 30, width: 90, height: 90), image: UIImage(named: "stick2")!))
        self.data[2].addSticker(SNSticker(frame: CGRect(x: 30, y: 100, width: 250, height: 250), image: UIImage(named: "stick3")!))
        self.data[3].addSticker(SNSticker(frame: CGRect(x: 20, y: 00, width: 140, height: 140), image: UIImage(named: "stick")!))
    }
    func updatePicture(_ newImage: UIImage) {
        createData(newImage)
        slider.reloadData()
    }
    
    fileprivate func setupSlider() {
        
        self.createData(pickedImage!)
        self.slider.dataSource = self
        self.slider.isUserInteractionEnabled = true
        self.slider.isMultipleTouchEnabled = true
        self.slider.isExclusiveTouch = false
        self.screenView.addSubview(slider)
        self.slider.reloadData()
    }
    
    func setupTextField() {
        
        self.screenView.addSubview(textField)
        self.tapGesture.delegate = self
        self.slider.addGestureRecognizer(tapGesture)
        
    }
    
    func setupButtonSave() {
        self.buttonSave.setAction {
            [weak weakSelf = self] in
            let picture = SNUtils.screenShot(weakSelf?.screenView)
            if let image = picture {
                //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddPostViewController") as? AddPostViewController {
                    
                    controller.pickedImage = image
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
            }
        }
        
        self.view.addSubview(self.buttonSave)
    }
    
    func setupButtonCamera() {
        self.buttonCamera.setAction {
            [weak weakSelf = self] in
            
            if let tmpImagePicker = weakSelf?.imagePicker {
                tmpImagePicker.allowsEditing = false
                tmpImagePicker.sourceType = .photoLibrary
                
                weakSelf?.present(tmpImagePicker, animated: true, completion: nil)
            }
        }
        
        imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        self.view.addSubview(self.buttonCamera)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FiltersViewController : UIGestureRecognizerDelegate {
    
    @objc func handleTap() {
        self.textField.handleTap()
    }
}

extension FiltersViewController: SNSliderDataSource {
    
    // The number of SNFilters that you want in the slider
    func numberOfSlides(_ slider: SNSlider) -> Int {
        return data.count
    }
    
    // For a given index, you return the corresponding SNFilter
    func slider(_ slider: SNSlider, slideAtIndex index: Int) -> SNFilter {
        return data[index]
    }
    
    // The starting index of the slider
    func startAtIndex(_ slider: SNSlider) -> Int {
        return 0
    }
}


// MARK: - UIImagePickerControllerDelegate Methods

extension FiltersViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            // To avoid too big images and orientation issue
            let newImage = pickedImage.resizeWithWidth(SNUtils.screenSize.width)
            if let image = newImage {
                updatePicture(image)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


