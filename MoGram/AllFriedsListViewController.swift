//
//  AllFriedsListViewController.swift
//  MoGram
//
//  Created by Mounika Nerella on 9/25/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import SVProgressHUD

class AllFriedsListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tblView: UITableView!
    
    var ref: DatabaseReference!
    var userArray = [UserModel]()
    var storage = Storage.storage()
    var friendsArray = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.navigationController?.navigationBar.isHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Users"
        ref = Database.database().reference()
        SVProgressHUD.show()
        tblView.tableFooterView = UIView()
        tblView.rowHeight = 220
        swipes()
        readUsers()
        // Do any additional setup after loading the view.
    }
    func swipes(){
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(rightSwipe)
        
    }
    
    @objc func swipeAction(swipe: UISwipeGestureRecognizer) {
        
        switch swipe.direction.rawValue{
        case 1:
            tabBarController?.selectedIndex -= 1
        case 2:
            tabBarController?.selectedIndex += 1
        default:
            break
        }
    }
    
    func readUsers() {
        
        self.ref.child("publicUsers").observe(.value, with: { (snapshot) in
            self.userArray = []
            self.friendsArray = []
            guard let users = snapshot.value as? Dictionary<String,[String: Any]> else{
                return
            }
            for user in users {
                
                let names = user.value
                
                if user.key ==  Auth.auth().currentUser?.uid {
                    let frnds = names["friends"] as? [String: Bool]
                    print(frnds)
//                    for frnd in frnds! {
//
//                        if frnd.value == true {
//
//                            self.friendsArray.append(frnd.key)
//                        }
//                    }
                }
                
                if user.key != Auth.auth().currentUser?.uid {
                    
                    if let fname = names["firstName"],let lname = names["lastName"] {
                        self.userArray.append(UserModel(fname: fname as! String, lname: lname as! String, userId: user.key))
                        print(self.userArray)
                    }
                }
            }
            
            SVProgressHUD.dismiss()
            self.tblView.reloadData()
        })
    }
    
    @IBAction func updateBtn(_ sender: UIBarButtonItem) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "UpdateViewController") as? UpdateViewController {
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func logOutBtn(_ sender: UIBarButtonItem) {
        
        let firebaseAuth = Auth.auth()
        do{
            try firebaseAuth.signOut()
        }catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
            return
        }
        
        print("User Signed Out")
        
        if let controller =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            present(controller, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "FriendsDisplayTableViewCell", for: indexPath) as? FriendsDisplayTableViewCell
        let userObj = userArray[indexPath.row]
       // print(userObj)
        let uid = userObj.uId
       // print(uid)
        let storageRef = storage.reference()
        let imageRef = storageRef.child("images/\(uid).jpg")
       // print(imageRef)
        cell?.lbltxt.text = userObj.firstName + " " + userObj.lastName
       // print(cell?.lbltxt.text)
        let addBtn = cell?.addFrndBtn
        addBtn?.tag = indexPath.row
        if friendsArray.contains(uid) {
            addBtn?.setTitle("Following", for: .normal)
            addBtn?.layer.borderWidth = 1
            addBtn?.setTitleColor(UIColor.lightGray, for: .normal)
            addBtn?.backgroundColor = UIColor.white
            addBtn?.isSelected = true
        }
        
        else{
            
            addBtn?.setTitle("Follow", for: .normal)
            addBtn?.backgroundColor = UIColor(displayP3Red: 0, green: 128/255, blue: 255/255, alpha: 1)
            addBtn?.setTitleColor(UIColor.white, for: .normal)
            addBtn?.layer.borderWidth = 0
            addBtn?.isSelected = false
        }
        
        addBtn?.addTarget(self, action: #selector(addAction), for: .touchUpInside)
        
        imageRef.getData(maxSize: 1 * 2048 * 1024) { data, error in
            
            if let error = error {
                print(error.localizedDescription)
            }
            else{
                cell?.imgview.image = UIImage(data: data!)
            }
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       print(userArray.count)
        return userArray.count
    }
    
    @objc func addAction(sender: UIButton) {
        
        let userObj = userArray[sender.tag]
        let userId = userObj.uId
        let currentUid = Auth.auth().currentUser!.uid
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            ref.child("publicUsers/\(currentUid)/friends/\(userId)").setValue(true)
            print("friend added")
            
        }
        else {
            ref.child("publicUsers/\(currentUid)/friends/\(userId)").setValue(false)
            print("Unfollowed")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
